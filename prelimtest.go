//Preliminary Test for Kulina Backend Developer
//Mohammad Qori Aziz Hakiki

package main

import (
	"fmt"
	"sort"
	"strconv"
	"strings"
)

//Function Soal 1
//Needs : Program that can convert inputted integer to an array of string, with conditions
//Input : Integer
//Output : Slice of string
func Soal1(input int) []string {
	var output = []string{}
	for i := 1; i <= input; i++ {
		if (i%3 == 0) && (i%5 == 0) {
			output = append(output, "Kulina x Food")
		} else if i%3 == 0 {
			output = append(output, "Kulina")
		} else if i%5 == 0 {
			output = append(output, "Food")
		} else {
			output = append(output, strconv.Itoa(i))
		}
	}
	return output
}

//Function Soal 2
//Needs : Find the dissimilarity between two string
//Input: Two strings
//Output: A letter, difference between two inputted strings
func Soal2(inputA string, inputB string) string { //Asumsi input ASCII 1 byte
	var output = ""
	//Sorting both of the input
	sortA := sortString(inputA)
	sortB := sortString(inputB)
	var found = false
	i := 0

	//Check whether A is empty or not
	if len(sortA) == 0 {
		output = sortB
	} else {
		//Find the difference
		for !(found) && i < len(sortA) {
			if sortA[i] != sortB[i] {
				output = string(sortB[i])
				found = true
			}
			i++
		}
	}
	return output
}

//Function to sort string
func sortString(input string) string {
	output := strings.Split(input, "")
	sort.Strings(output)
	return strings.Join(output, "")
}

//Function Soal 3
//Needs : Convert roman numeral string to integer
//Input: A string, roman numeral format
//Output: An integer, converted from roman numeral
func Soal3(input string) int {
	//Initialize dictionary
	var roman = map[string]int{
		"I": 1,
		"V": 5,
		"X": 10,
		"L": 50,
		"C": 100,
		"D": 500,
		"M": 1000,
	}

	procString := strings.Split(input, "")
	//Processing from far right side of the string
	output := roman[procString[len(procString)-1]]
	for i := len(procString) - 2; i >= 0; i-- {
		if roman[procString[i]] >= roman[procString[i+1]] {
			output += roman[procString[i]] //Add if the left element is same as or bigger than right element
		} else {
			output -= roman[procString[i]] //Substract
		}
	}
	return output
}

func main() {
	//Simulasi soal 1
	fmt.Printf("%q", Soal1(15))
	fmt.Println("")

	//Simulasi soal 2
	fmt.Println(Soal2("bxcn", "abncx"))
	fmt.Println(Soal2("", "y"))
	fmt.Println(Soal2("annqalff", "fqlnannaf"))

	//Simulasi soal 3
	fmt.Println(Soal3("XII"))
	fmt.Println(Soal3("LIV"))
	fmt.Println(Soal3("MMXXII"))
}
