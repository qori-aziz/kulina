# This is the answer for Kulina Backend Engineer preliminary test.

## Problem 1
1. Needs: Program that can convert inputted integer to an array of string, with conditions <br>
    * If the current integer divisible by 3, insert “Kulina” to array <br>
    * If it divisible by 5, insert “Food” to array <br>
    * If it divisible by 3 and 5, insert “Kulina x Food” to array <br>
    * Otherwise, insert integer as string to array <br>
    * Assumption: Input always right (no error handler needed) <br>
2. Input: an integer variable <br>
3. Output: Array of string, consisted of converted integer with the conditions above <br>

## Problem 2
1. Needs: Program that can find the difference between two inputted strings, with:
    * Second string is a shuffled version of first string
    * Difference is only one letter
    * Assumption: Input is always right (no error handler needed)
    * Return the letter difference
2. Input: Two strings
3. Output: A letter, difference between two inputted strings

## Problem 3
1. Needs: Program that can convert inputted Roman Numeral string to integer
    * Assumption: Input always right (no error handler needed)
2. Input: a string in roman numeral format
3. Output: Integer, converted from roman numeral string
